# Follow-Me Robot: NessieBot!

## Description
This project is an assignment from [Noroff](https://www.noroff.no/en/), as part of [Experis Academy for Embedded Development](https://www.experis.se/sv/it-tjanster/experis-academy). 
<br />
The robot is based upon the [JETANK AI Kit](https://www.waveshare.com/jetank-ai-kit.htm) from Waveshare. You can watch a demonstration of our prototype [HERE](https://www.youtube.com/watch?v=ELAELLxKeWs).

## Table of Contents
- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Contributors](#contributing)
- [References](#references)
- [License](#license)

## Background
The project is made with JETANK AI Kit based on the Jetson Nano development board, OpenCV and Python. The robot tracks lowerbody and will follow if the person moves to the right or left, and also move forward when the person is in a certain distance from the robot. The name NessieBot is inspired by one of the developers cute dog Nessie!
<br />

### Facial Recognitioning
We also made a model to track facial recognitioning, we wanted to use this as a starting point for the follow-me robot. The robot should only be possible to start if it was its "owner" present in the camera. The use case for this would be:
- Put the robot on a bench or higher up
- Look into the camera
- If the correct person present, start tracking the movements and follow!

We could not install the proper OpenCV version on JETANK though, but we have included the model as file named ```LBPH_model.ipynb```
<br />
The model is currently only letting Princess Leia use the NessieBot!

## Install
- JETANK AI Kit from Waveshare
- OpenCV (contrib version for LBPH model)
- Python
- Numpy
- os
- PIL


## Usage
Upload the code to your JETANK folder and run it.

## Contributors
This is a project created by [Helena Barmer](https://gitlab.com/helenabarmer), [Martin Dahrén](https://gitlab.com/MarDah) and [Joel Fredin](https://gitlab.com/joelfredin).


## License
[MIT]()